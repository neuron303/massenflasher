#!/usr/bin/python

# Written by overflo  Early 2024
# Published at https://gitlab.com/overflo23/massenflasher
# LICENSE: \o/ free as in freedom

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os
import subprocess
import sys

processes = []


if len(sys.argv) < 2:
    print("Run like: ", str(sys.argv[0]), " <fullimage.bin>")
    sys.exit()

img = sys.argv[1]


class Handler(FileSystemEventHandler):
    def on_created(self, event):
        if "ttyA" in event.src_path:
            dev = event.src_path
            print("FLASHING ", dev)
            p = subprocess.Popen(
                [
                    "/usr/local/bin/esptool.py",
                    "--chip",
                    "esp32-s2",
                    "--port",
                    dev,
                    "--baud",
                    "921600",
                    "write_flash",
                    "0",
                    img,
                ]
            )
            os.spawnl(os.P_WAIT, "/usr/bin/say", "say", "WALLAH BRUDI!")
            processes.append(p)
            (output, err) = p.communicate()
            os.spawnl(os.P_WAIT, "/usr/bin/say", "say", "UNPLUG ME PLEASE")


if __name__ == "__main__":
    path = "/dev/"
    event_handler = Handler()

    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while observer.is_alive():
            observer.join(1)
            print("Waiting for /dev/ttyA* to appear")

    except KeyboardInterrupt:
        print("Observer stopping")
    finally:
        observer.stop()
        observer.join()
